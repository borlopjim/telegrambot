/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.handlers;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import putobot.BotConfig;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.api.objects.Message;
import putobot.SendUtils;
import putobot.memes.MemeCommands;
import putobot.exceptions.NotAnAudioException;
import putobot.exceptions.NotAStickerException;
import putobot.exceptions.NotAGifException;

/**
 *
 * @author putorojo
 */
public class PutoHandler extends TelegramLongPollingBot {

    
    private Message msg;
    private MemeCommands command;
    private String arguments;
    private String msgStr;
    private final SendUtils send;

    public PutoHandler() {
        send = new SendUtils();
    }

    @Override
    public String getBotUsername() {
        return BotConfig.getPUTOBOT_NAME();
    }

    @Override
    public void onUpdateReceived(Update update) {
        String strCommand;
        if (update.hasMessage()) {
            msg = update.getMessage();

            printMessages();

            msgStr = msg.getText();
            if (msgStr.substring(0, 1).equals("/")) {
                try {
                    strCommand = msgStr.substring(1).split(" ")[0].split("@PutoRojoBot")[0];
                    if(MemeCommands.isAMeme(strCommand)){
                        command = MemeCommands.valueOf(strCommand);
                        arguments = msgStr.split(" ", 2).length > 1 ? msgStr.split(" ", 2)[1] : "";
                        switch (command) {
                            case lenny:
                                send.sendMessage("( ͡° ͜ʖ ͡°)", this, msg);
                                break;
                            case carmena:
                                send.sendMessage("Mi hija de 6 años: " + arguments 
                                        + ". No te perdonaré jamás Manuela Carmena, jamás", this, msg);
                                break;
                            case energy:
                                send.sendMessage("༼ つ ◕_◕ ༽つ  " + arguments 
                                        + " TAKE MY ENERGY ༼ つ ◕_◕ ༽つ", this, msg);
                                break;
                            case fangay:
                                send.sendMessage("WHERE ARE YOU NOW " + arguments + " FANGAYS!?", this, msg);
                                break;
                            case wow:                            
                            case nooo:
                                send.sendSticker(command.getSticker(), this, msg);
                                send.sendAudio(command.getAudio(), this, msg);
                                break;
                            case hohohaha:
                            case kappa:
                            case cojones:
                            case kappaross:
                                send.sendSticker(command.getSticker(), this, msg);
                                break;                        
                            case help:
                                send.sendMessage(BotConfig.getPUTOBOT_HELP(), this, msg);
                                break;
                            case rekt:
                                send.sendGif(command.getGif(), this, msg);
                                break;
                            case salida:
                                send.sendMessageToChannel(arguments, command.getChannelID(), this);
                                break;
                        }
                    }
                } catch (NotAStickerException | NotAnAudioException | NotAGifException | TelegramApiException ex) {
                    System.err.println("ERROR INESPERADO");
                    Logger.getLogger(PutoHandler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }

    @Override
    public String getBotToken() {
        return BotConfig.getPUTOBOT_TOKEN();
    }

    public void printMessages() {

        System.out.print(msg.toString());
        
        System.out.print("[" + msg.getFrom().getFirstName() + "]");
        System.out.print("[" + msg.getChatId() + "]");
        System.out.print("[" + msg.getFrom().getId() + "]: ");

        if (msg.hasText()) {

            String msgText = msg.getText();

            System.out.println(msgText);

        } else if (msg.getSticker() != null) {
            System.out.println(msg.getSticker().toString());
        }

    }


    
}
