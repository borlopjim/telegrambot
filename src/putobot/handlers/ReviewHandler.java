/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.handlers;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import putobot.BotConfig;
import putobot.SendUtils;
import putobot.reviews.ReviewCommands;
import putobot.reviews.db.DataAccess;

/**
 *
 * @author putorojo
 */
public class ReviewHandler extends TelegramLongPollingBot {

    public final DataAccess db;
    private Message msg;
    private String msgStr;
    private String arguments;
    private ReviewCommands command;
    private final SendUtils send;
    private int replyID;
    private TreeMap<Integer, String> reviewMap;
    private boolean review, setReview, newReview;
    private String reviewName;
    private String category;
    private int id;

    public ReviewHandler() throws SQLException {
        db = new DataAccess();
        send = new SendUtils();
        review = false;
        setReview = false;
        newReview = false;
        reviewMap = null;
        reviewName = "";
        id = 0;
        replyID = 0;
    }

    @Override
    public String getBotToken() {
        return BotConfig.getREVIEWBOT_TOKEN();
    }

    @Override
    public void onUpdateReceived(Update update) {
        int nota = 27;
        String strCommand;
        if (update.hasMessage()) {
            msg = update.getMessage();
            msgStr = msg.getText();
            try {
                if (((msg.getReplyToMessage() != null && msg.getReplyToMessage().getMessageId() == replyID)) && review) {
                    if (isInteger(msg.getText().trim())){
                        id = Integer.parseInt(msg.getText().trim());
                        if(id > 0 && id < reviewMap.lastKey()){
                            reviewName = reviewMap.get(id);
                            replyID = send.sendMessage("Responde a este mensaje "
                                    + "con tu nota (de 0 a 10)", this, msg).getMessageId();
                            review = false;
                            newReview = true;
                        }else if(id == reviewMap.lastKey()){
                            replyID = send.sendMessage("Responde a este mensaje con el nombre de lo "
                                    + "que quieres valorar.", this, msg).getMessageId();
                            review = false;
                            setReview = true;
                            
                        }else{
                            send.sendMessage("Introduce una opción válida", this, msg);
                        }
                    }
                } else if ((msg.getReplyToMessage() != null && msg.getReplyToMessage().getMessageId() == replyID) && setReview) {
                    reviewName = msg.getText();
                     replyID = send.sendMessage("Responde a este mensaje "
                                    + "con tu nota (de 0 a 10)", this, msg).getMessageId();
                            setReview = false;
                            newReview = true;
                } else if ((msg.getReplyToMessage() != null && msg.getReplyToMessage().getMessageId() == replyID) && newReview) {
                    if(isInteger(msg.getText().trim()))
                        nota = Integer.parseInt(msg.getText().trim());
                        if(nota > -1 && nota <11){
                            if(db.newReview(msg.getFrom().getFirstName(), msg.getFrom().getLastName(),
                                    reviewName, nota, category)){
                                send.sendMessage("Tu valoración se ha añadido correctamente", this, msg);
                            }else{
                                send.sendMessage("Tu valoración no se ha añadido.", this, msg);
                            }
                            reviewName = "";
                            category = "";
                            nota = 0;
                            newReview = false;
                        }
                }
                if (msgStr.substring(0, 1).equals("/")) {
                    strCommand = msgStr.substring(1).split(" ")[0].split("@PutoRojoBot")[0];
                    if (ReviewCommands.isAReviewCommand(strCommand)) {
                        command = ReviewCommands.valueOf(msgStr.substring(1).split(" ")[0].split("@PutoRojoBot")[0]);
                        arguments = msgStr.split(" ", 2).length > 1 ? msgStr.split(" ", 2)[1] : "";
                        switch (command) {
                            case valorar:
                                newReview(arguments);
                                break;
                            case valoraciones:
                                send.sendMessage(db.getReviews(arguments.toUpperCase()), this, msg);
                                break;
                            case categorias:
                                sendCategories();
                                break;
                            case newcategoria:
                                newCategory(arguments);
                                break;
                            case help:
                                send.sendMessage(BotConfig.getREVIEWBOT_HELP(), this, msg);
                        }
                    }
                }
            } catch (TelegramApiException ex) {
                Logger.getLogger(ReviewHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void newCategory(String arguments) throws TelegramApiException {
        arguments = arguments.toUpperCase();
        if (db.isACategory(arguments)) {
            send.sendMessage("Ya existe la categoría: " + arguments, this, msg);
        } else {
            db.newCategory(arguments);
            send.sendMessage("Se ha creado la categoría: " + arguments, this, msg);
        }
    }

    private void sendCategories() throws TelegramApiException {
        send.sendMessage(db.getCategories(), this, msg);
    }

    @Override
    public String getBotUsername() {
        return BotConfig.getREVIEWBOT_NAME();
    }
    
    private void sendReviews(String arguments) throws TelegramApiException{
        
    }

    private void newReview(String arguments) throws TelegramApiException {
        arguments = arguments.toUpperCase();
        if (arguments.equals("")) {
            send.sendMessage("Dime la categoría a la que quieres añadir una valoración.", this, msg);
        } else if (db.isACategory(arguments)) {
            category = arguments;
            reviewMap = db.getReviewList(arguments);
            replyID = send.sendMessage("Responde a este mensaje con el número de la opción que quieres:\n"
                    + toString(reviewMap), this, msg).getMessageId();
            review = true;
        } else {
            send.sendMessage(arguments + " no es una categoría.", this, msg);
        }
    }

    private String toString(TreeMap<Integer, String> map) {
        String mapString = "";
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            mapString += key + ".- " + value + "\n";
        }
        return mapString;
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
