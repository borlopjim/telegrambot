/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot;

import org.telegram.telegrambots.api.methods.send.SendAudio;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendSticker;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author putorojo
 */
public class SendUtils {
    
    private final SendMessage send;
    private final SendSticker sticker;
    private final SendAudio audio;
    private final SendDocument gif;
    
    public SendUtils(){
        
        send = new SendMessage();
        sticker = new SendSticker();
        audio = new SendAudio();
        gif = new SendDocument();
    }
    
    public Message sendSticker(String stickerId, TelegramLongPollingBot bot, Message msg) throws TelegramApiException {
        sticker.setChatId(msg.getChatId().toString());
        sticker.setReplyToMessageId(getMessageId(msg));
        sticker.setSticker(stickerId);
        return bot.sendSticker(sticker);
    }

    public Message sendMessage(String text, TelegramLongPollingBot bot, Message msg) throws TelegramApiException {
        send.setChatId(msg.getChatId().toString());
        send.setText(text);
        send.setReplyToMessageId(getMessageId(msg));
        
        return bot.sendMessage(send);
    }

    public Message sendAudio(String audioId, TelegramLongPollingBot bot, Message msg) throws TelegramApiException {
        
        audio.setChatId(msg.getChatId().toString());
        audio.setAudio(audioId);
        audio.setReplyToMessageId(getMessageId(msg));
        return bot.sendAudio(audio);
    }
    
    public void sendGif(String gifId, TelegramLongPollingBot bot, Message msg) throws TelegramApiException{
        
        gif.setChatId(msg.getChatId().toString());
        gif.setDocument(gifId);
        gif.setReplyToMessageId(getMessageId(msg));
        bot.sendDocument(gif);
    }
    
    public void sendMessageToChannel(String text, String chatID, TelegramLongPollingBot bot) throws TelegramApiException{
        send.setChatId(chatID);
        send.setText(text);
        
        bot.sendMessage(send);
    }
    
    private Integer getMessageId(Message msg){
        return  msg.getReplyToMessage() instanceof Message ? msg.getReplyToMessage().getMessageId() : msg.getMessageId();
    }
}
