/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.reviews.db;

/**
 *
 * @author putorojo
 */
public class DBConfig {
    public static final String DB_HOST = "dbhost";
    public static final String DB_USERNAME = "user";
    public static final String DB_PASSWORD = "password";
    public static final String DB_DBNAME = "dbname";
}
