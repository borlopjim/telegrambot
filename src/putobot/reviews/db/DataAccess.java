/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.reviews.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author putorojo
 */
public class DataAccess {

    Connection db;

    public DataAccess() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            db = DriverManager.getConnection("jdbc:mysql://"
                    + DBConfig.DB_HOST + "/" + DBConfig.DB_DBNAME, DBConfig.DB_USERNAME, DBConfig.DB_PASSWORD);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void close() throws SQLException {
        db.close();
    }

    public String getCategories() {
        String query = "SELECT * FROM category ORDER BY ID ASC;";
        String categories = "";
        Statement stmnt;
        ResultSet set;
        try {
            stmnt = db.createStatement();
            set = stmnt.executeQuery(query);
            while (set.next()) {
                String name = set.getString("name");
                categories += set.getInt("ID") + ".- " + name.substring(0,1) + name.substring(1).toLowerCase() + "\n";
            }
            stmnt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categories;
    }

    public void newCategory(String name) {
        String query = "INSERT INTO category (name) VALUES(?)";
        PreparedStatement stmnt;
        try {
            stmnt = db.prepareStatement(query);
            stmnt.setString(1, name);
            stmnt.executeUpdate();
            stmnt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isACategory(String name) {
        String query = "SELECT name FROM category WHERE name = ?";
        PreparedStatement stmnt;
        ResultSet set;
        try {
            stmnt = db.prepareStatement(query);
            stmnt.setString(1, name);
            set = stmnt.executeQuery();
            if (set.first()) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public TreeMap getReviewList(String arguments) {
        TreeMap<Integer,String> reviewMap = new TreeMap<>();
        String query = "SELECT DISTINCT r.nombre FROM review r, category c "
                + "WHERE r.categoria = c.ID AND c.name = ? ORDER BY r.nombre ASC;";
        PreparedStatement stmnt;
        ResultSet set;
        int id = 1;
        try {
            stmnt = db.prepareStatement(query);
            stmnt.setString(1, arguments);
            set = stmnt.executeQuery();
            while (set.next()) {
                reviewMap.put(id, set.getString("nombre"));
                id++;
            }
            stmnt.close();
            reviewMap.put(id, "Nueva Valoración");
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reviewMap;
    }

    public boolean newReview(String firstName, String lastName, String reviewName, int nota, String category) {
        String name;
        boolean isOk = false;
        if(lastName != null && lastName.equals("Seijas Bellido"))
            name = "Seijas";
        else if(lastName != null && lastName.equals("Asencio"))
            name = "Nono";
        else
            name = firstName;
        
        String query = "INSERT INTO review (nombre, user, nota, categoria)"
                + "VALUES (?,?,?, (SELECT ID FROM category WHERE name = ?));";
        PreparedStatement stmnt;
        try {
            stmnt = db.prepareStatement(query);
            stmnt.setString(1, reviewName);
            stmnt.setString(2, name);
            stmnt.setInt(3, nota);
            stmnt.setString(4, category);
            isOk = stmnt.executeUpdate() > 0;
            stmnt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return isOk;
        
    }
    
    public String getReviews(String category){
        String reviews = "";
        String query = "SELECT r.nombre, AVG(r.nota) FROM review r, category c"
                + " WHERE r.categoria = c.ID AND c.name = ? GROUP BY nombre";
        PreparedStatement stmnt;
        ResultSet set;
        int count = 1;
        try{
            stmnt = db.prepareStatement(query);
            stmnt.setString(1, category);
            set = stmnt.executeQuery();
            while (set.next()){
                reviews += count +".- "+set.getString("nombre")+" - "
                        +String.format("%.2f",set.getFloat("AVG(r.nota)")) + " "+
                        getUsers(set.getString("nombre"),category)+"\n";
                count++;
            }
        }catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reviews;
    }
    
    public String getUsers(String nombre, String categoria){        
        String names = "(";
        String query = "SELECT r.user FROM review r, category c"
                + " WHERE r.categoria = c.ID AND c.name = ? AND r.nombre = ?";
        PreparedStatement stmnt;
        ResultSet set;
        try{
            stmnt = db.prepareStatement(query);
            stmnt.setString(1,categoria);
            stmnt.setString(2, nombre);
            set = stmnt.executeQuery();
            while (set.next()) {
                if(set.isLast())
                    names += set.getString("user");
                else
                    names += set.getString("user")+", ";
            }
            names += ")";
        }catch(SQLException ex){
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return names;
    }

}
