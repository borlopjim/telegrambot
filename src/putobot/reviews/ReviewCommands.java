/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.reviews;

/**
 *
 * @author putorojo
 */
public enum ReviewCommands {
    
    valorar,
    valoraciones,
    newcategoria,
    help,
    categorias;
    
    public static boolean isAReviewCommand(String command){
        for(ReviewCommands c : ReviewCommands.values()){
            if(c.name().equals(command))
                return true;
        }
        return false;
    }
    
}
