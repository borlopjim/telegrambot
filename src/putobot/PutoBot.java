/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot;

import java.sql.SQLException;
import putobot.handlers.PutoHandler;
import putobot.handlers.ReviewHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;

/**
 *
 * @author putorojo
 */
public class PutoBot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TelegramBotsApi bot = new TelegramBotsApi();
        
        try {
            bot.registerBot(new ReviewHandler());
            bot.registerBot(new PutoHandler());
        } catch (TelegramApiException | SQLException ex) {
            Logger.getLogger(PutoBot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
