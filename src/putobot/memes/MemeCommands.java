/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.memes;

import putobot.exceptions.NotAGifException;
import putobot.exceptions.NotAStickerException;
import putobot.exceptions.NotAnAudioException;

/**
 *
 * @author putorojo
 */
public enum MemeCommands {
    help,
    lenny,
    energy,
    kappa,
    hohohaha,
    fangay,
    wow,
    nooo,
    carmena,
    kappaross,
    cojones,
    salida,
    rekt;
    
    private static final String KAPPA_STICKER = "BQADAgADGgADVaMOAAFpsDjXHc8RCwI";
    private static final String HOHOHAHA_STICKER = "BQADBAADLQADGQsgAAES5UFqGd82fQI";
    private static final String NOTAIL_STICKER = "BQADBAADdgADGQsgAAEBY1vVaQ8AAaoC";
    private static final String WOW_STICKER = "BQADBAADSgADGQsgAAE2e0l4scD-NAI";
    private static final String NOOO_STICKER = "BQADAQADKAADheAvCM6sXl4iYQ9zAg";
    private static final String KAPPA_ROSS = "BQADBAADsQADGQsgAAGTWgsemG1miwI";
    private static final String COJONES = "BQADBAADswADGQsgAAFq20AuXbhhOQI";
    
    private static final String NOOO_AUDIO = "BQADBAADPwADCtuRCCbhmxnFeNIGAg";
    private static final String WOW_AUDIO = "BQADBAADHwADCtuRCM-fjh1BtlMHAg";
    
    private static final String REKT_GIF = "BQADBAADWwgAAk4YZAcM3cfEugE0NgI";
    
    private static final String SALIDA_ID = "-1001065922802";
    
    public String getSticker() throws NotAStickerException{
        switch(this){
            case kappa:
                return KAPPA_STICKER;
            case hohohaha:
                return HOHOHAHA_STICKER;
            case wow:
                return WOW_STICKER;
            case nooo:
                return NOOO_STICKER;
            case kappaross:
                return KAPPA_ROSS;
            case cojones:
                return COJONES;
        }
        throw new NotAStickerException("El sticker no existe");
    }
    
    public String getAudio() throws NotAnAudioException{
        switch(this){
            case nooo:
                return NOOO_AUDIO;
            case wow:
                return WOW_AUDIO;
        }
        throw new NotAnAudioException("No existe el audio");
    }
    
    public String getGif() throws NotAGifException{
        switch(this){
            case rekt:
                return REKT_GIF;
        }
        throw new NotAGifException("No existe el gif");
    }
    
    public String getChannelID(){
        return SALIDA_ID;
    }
    
    public static boolean isAMeme(String command){
        for(MemeCommands c : MemeCommands.values()){
            if(c.name().equals(command))
                return true;
        }
        return false;
    }
    
}
