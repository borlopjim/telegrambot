/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package putobot.exceptions;

/**
 *
 * @author putorojo
 */
public class NotAStickerException extends Exception {

    public NotAStickerException(String msg) {
        super(msg);
    }

}
